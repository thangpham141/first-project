<!DOCTYPE html>
<html lang="en">
<head>
    <title>Đăng nhập</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>
<?php
include $_SERVER['DOCUMENT_ROOT'].'\MVC\Models\Model Form\model_sign_in.php';
?>
<div class="container border col-sm-3 mt-3">
    <h1>Đăng nhập</h1>
    <hr>
    <div class="">
        <form action="" method="post">
            <div class="form-group pt-3">
                <label for="account_name">Tên tài khoản:</label>
                <input type="text" class="form-control" id="account_name" placeholder="Nhập tên tài khoản" name="account_name">
            </div>
            <div class="form-group">
                <label for="password">Mật khẩu:</label>
                <input type="password" class="form-control" id="password" placeholder="Nhập mật khẩu" name="password">
            </div>
            <button type="submit" class="btn btn-primary mb-2" name="sign_in">Đăng nhập</button>
            <button class="btn btn-success mb-2" onclick="window.open('../../../index.php')">Quay lại trang chủ</button>
        </form>
    </div>
</div>

</body>
</html>
