<!DOCTYPE html>
<html lang="en">
<head>
    <title>Đăng ký</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container mt-1">
    <div class="">
        <form method="post" action="">
            <div class="form-group d-flex pt-2">
                <label class="col-sm-2  pt-2" for="full_name">Họ và tên:</label>
                <input type="text" class="form-control col-sm-6" id="full_name" placeholder="Nhập họ và tên" name="full_name">
                <span class="col-sm-3 pt-2">
                </span>
            </div>
            <div class="form-group d-flex">
                <label class="col-sm-2  pt-2" for="gender">Giới tính:</label><br>
                <label>
                    <select name="gender" id="gender" class="form-control">
                        <option value="male">Nam</option>
                        <option value="female">Nữ</option>
                        <option value="other">Khác</option>
                    </select>
                </label>
            </div>
            <div class="form-group d-flex">
                <label class="col-sm-2 pt-2" for="date_of_birth">Ngày sinh:</label>
                <input type="date" class="form-control col-sm-6" id="date_of_birth" name="date_of_birth">
            </div>
            <div class="form-group d-flex">
                <label class="col-sm-2 pt-2" for="address">Địa chỉ:</label>
                <input type="text" class="form-control col-sm-6" id="address" placeholder="Nhập địa chỉ" name="address">
            </div>
            <div class="form-group d-flex">
                <label class="col-sm-2 pt-2" for="email">Email:</label>
                <input type="email" class="form-control col-sm-6" id="email" placeholder="Nhập email" name="email">
                <span class="col-sm-3 pt-2">
                </span>
            </div>
            <div class="form-group d-flex">
                <label class="col-sm-2 pt-2" for="phone_number">Số điện thoại:</label>
                <input type="text" class="form-control col-sm-6" id="phone_number" placeholder="Nhập số điện thoại" name="phone_number">
                <span class="col-sm-3 pt-2">
                </span>
            </div>

            <button type="submit" class="btn btn-primary mb-2" name="add_user">Đăng ký</button>
            <button class="btn btn-success mb-2" onclick="window.open('../../../index.php')" formtarget="_self">Quay lại trang chủ</button>
        </form>
    </div>
</div>

</body>
</html>
