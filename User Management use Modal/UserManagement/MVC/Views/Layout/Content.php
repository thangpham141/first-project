<!DOCTYPE html>
<html lang="en">
<head>
    <title></title>
<!--    <script src="../../../UserManagement/Public/Library/jquery-3.6.0.min.js"></script>-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body>
<div class="">

    <div class="content-title pb-2"><h3>Danh sách người dùng</h3></div>
    <div class="content-body">

    </div>
    <div class="content-footer">
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Họ và tên</th>
                <th>Giới tính</th>
                <th>Ngày sinh</th>
                <th>Địa chỉ</th>
                <th>Email</th>
                <th>Số điện thoại</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php
            require_once $_SERVER['DOCUMENT_ROOT'].'\MVC\Models\Model Common\model_get_info_user.php';
            require_once $_SERVER['DOCUMENT_ROOT'].'\MVC\Models\Model Validate\validate_info_user.php';
            //include $_SERVER['DOCUMENT_ROOT'].'\MVC\Models\Model DataLayer\db_query.php';
            $listUser = GetListUser();
            foreach ($listUser as $user){
                $id = $user['UserID'];
                echo "
                        <tr>
                            <td>".$user['FullName']."<span  id='userID' class='d-none'>$id</span></td>
                            <td>".ValGender($user['Gender'])."</td>
                            <td>".ValDOB($user['DateOfBirth'])."</td>
                            <td>".$user['Address']."</td>
                            <td>".$user['Email']."</td>
                            <td>".$user['PhoneNumber']."</td>
                            <td>
                            <button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#updateModal\" onclick='getInfoUser($id)'>"."
                                 Sửa
                            </button>

                                <button class='btn btn-danger' onclick=\"window . open('./MVC/Views/Form/form_delete.php?id=$id', '_self')\">Xóa</button>
                            </td>
                        </tr>
                        ";
            }
            ?>

            </tbody>
        </table>

    </div>
</div>
<div class="modal" id="updateModal">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Thông tin người dùng</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form method="post" action="">
                    <div class="form-group d-flex pt-2">
                        <label class="col-sm-2  pt-2" for="full_name">Họ và tên:</label>
                        <input type="text" class="form-control col-sm-6" id="full_name" placeholder="Nhập họ và tên" name="full_name">
                        <span class="col-sm-3 pt-2">
                </span>
                    </div>
                    <div class="form-group d-flex">
                        <label class="col-sm-2  pt-2" for="gender">Giới tính:</label><br>
                        <label>
                            <select name="gender" id="gender" class="form-control">
                                <option value="male" id="male">Nam</option>
                                <option value="female" id="female">Nữ</option>
                                <option value="other" id="other">Khác</option>
                            </select>
                        </label>
                    </div>
                    <div class="form-group d-flex">
                        <label class="col-sm-2 pt-2" for="date_of_birth">Ngày sinh:</label>
                        <input type="date" class="form-control col-sm-6" id="date_of_birth" name="date_of_birth">
                    </div>
                    <div class="form-group d-flex">
                        <label class="col-sm-2 pt-2" for="address">Địa chỉ:</label>
                        <input type="text" class="form-control col-sm-6" id="address" placeholder="Nhập địa chỉ" name="address">
                    </div>
                    <div class="form-group d-flex">
                        <label class="col-sm-2 pt-2" for="email">Email:</label>
                        <input type="email" class="form-control col-sm-6" id="email" placeholder="Nhập email" name="email">
                        <span class="col-sm-3 pt-2">
                </span>
                    </div>
                    <div class="form-group d-flex">
                        <label class="col-sm-2 pt-2" for="phone_number">Số điện thoại:</label>
                        <input type="text" class="form-control col-sm-6" id="phone_number" placeholder="Nhập số điện thoại" name="phone_number">
                        <span class="col-sm-3 pt-2">
                </span>
                    </div>

                    <button type="submit" class="btn btn-primary mb-2" name="add_user">Đăng ký</button>
                    <button class="btn btn-success mb-2" onclick="window.open('../../../index.php')" formtarget="_self">Quay lại trang chủ</button>
                </form>
<!--                --><?php
//                require_once $_SERVER['DOCUMENT_ROOT'].'\MVC\Views\Form\form_info_user.php';
//                ?>
            </div>
        </div>
    </div>
</div>
<script>
    $("#full_name").val("aaaaa");
    function getInfoUser(id){
        // $.get('../../UserManagement/MVC/Models/Model DataLayer/model_get_user.php',{'id':id},function (data){
        //     var infoUser = JSON.parse(data);
        //     alert(infoUser);
        // });
        $.ajax({
            url: '../../UserManagement/MVC/Models/Model DataLayer/model_get_user.php',
            type: 'get',
            data: {'id':id},
            success: function (data){
                data = data.split('/');
                $(".modal-body #full_name").val(data[0]);
                if(data[1]==1){
                    $(".modal-body #gender").val("male");
                }
                else if(data[1]==2){
                    $(".modal-body #gender").val("female");
                }
                else {
                    $(".modal-body #gender").val("other");
                }
                $(".modal-body #date_of_birth").val(data[2]);
                $(".modal-body #address").val(data[3]);
                $(".modal-body #email").val(data[4]);
                $(".modal-body #phone_number").val(data[5]);
            }
        });
    }
</script>
</body>
</html>

