<!DOCTYPE html>
<html lang="en">
<head>
    <title>Quản lý người dùng</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>
<?php
//require_once $_SERVER['DOCUMENT_ROOT'].'\MVC\Models\Model DataLayer\db_query.php';
//?>

<div class="d-flex">
    <div class="float-left col-sm-10 mt-3">
        <h2>Quản lý người dùng</h2></div>
            <!-- Button to Open the Modal -->
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                Đăng ký
            </button>

            <!-- The Modal -->
            <div class="modal" id="myModal">
                <div class="modal-dialog modal-xl">
                    <div class="modal-content">

                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">Đăng ký</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <!-- Modal body -->
                        <div class="modal-body">
                            <?php
                            require_once $_SERVER['DOCUMENT_ROOT'].'\MVC\Views\Form\form_sign_up.php';
                            ?>
                        </div>
                    </div>
                </div>
            </div>





        <!--        <button class="btn btn-success mt-2" onclick="window.open('./MVC/Views/Form/form_sign_up.php','_self')">Đăng ký</button>-->
        <!--        <button class="btn btn-success mt-2" onclick="window.open('./MVC/Views/Form/form_sign_in.php','_self')">Đăng nhập</button>-->
</div>
<hr>
</body>
</html>
