<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>
<div class="m-5">

    <div class="content-title pb-2"><h3>Danh sách người dùng</h3></div>
    <div class="content-body">

    </div>
    <div class="content-footer">
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Họ và tên</th>
                <th>Giới tính</th>
                <th>Ngày sinh</th>
                <th>Địa chỉ</th>
                <th>Email</th>
                <th>Số điện thoại</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php
            require_once $_SERVER['DOCUMENT_ROOT'].'\MVC\Models\Model Common\model_get_info_user.php';
            require_once $_SERVER['DOCUMENT_ROOT'].'\MVC\Models\Model Validate\validate_info_user.php';
            //include $_SERVER['DOCUMENT_ROOT'].'\MVC\Models\Model DataLayer\db_query.php';
            $listUser = GetListUser();
            foreach ($listUser as $user){
                $id = $user['UserID'];
                echo "
                        <tr>
                            <td>".$user['FullName']."</td>
                            <td>".ValGender($user['Gender'])."</td>
                            <td>".ValDOB($user['DateOfBirth'])."</td>
                            <td>".$user['Address']."</td>
                            <td>".$user['Email']."</td>
                            <td>".$user['PhoneNumber']."</td>
                            <td>
                                <button onclick=\"window.open('./MVC/Views/Form/form_info_user.php?id=$id','_self')\" type='button' class='btn btn-primary' >
                                Sửa
                                </button>
                                <button class='btn btn-danger' onclick=\"window . open('./MVC/Views/Form/form_delete.php?id=$id', '_self')\">Xóa</button>
                            </td>
                        </tr>
                        ";
            }
            ?>

            </tbody>
        </table>

    </div>
</div>
</body>
</html>
