<!DOCTYPE html>
<html lang="en">
<head>
    <title>Quản lý người dùng</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>
<?php
require_once $_SERVER['DOCUMENT_ROOT'].'\MVC\Models\Model DataLayer\db_query.php';
?>

<div class="d-flex">
    <div class="float-left col-sm-10 mt-3">
        <h2>Quản lý người dùng</h2></div>
    <div class="float-right mt-2">
                <button class="btn btn-success mt-2" onclick="window.open('./MVC/Views/Form/form_sign_up.php','_self')">Đăng ký</button>
                <button class="btn btn-success mt-2" onclick="window.open('./MVC/Views/Form/form_sign_in.php','_self')">Đăng nhập</button>
    </div>
</div>
<hr>
</body>
</html>