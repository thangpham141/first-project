<!DOCTYPE html>
<html lang="en">
<head>
    <title>Thông tin người dùng</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>
    <div class="container border col-sm-3 mt-5 bg-light">
        <h3 class="pt-2">Bạn có chắc chắn xóa người dùng này!</h3>
        <hr>
        <form method="post">
            <button class="btn btn-success" name="delete_user">Có</button>
            <button class="btn btn-danger" name="cancel">Hủy</button>
            <?php
            require_once $_SERVER['DOCUMENT_ROOT'].'\MVC\Models\Model Form\model_delete.php';
            ?>
        </form>
    </div>
</body>
</html>