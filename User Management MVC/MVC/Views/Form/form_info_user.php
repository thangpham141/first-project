<!DOCTYPE html>
<html lang="en">
<head>
    <title>Thông tin người dùng</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>
<?php
require_once $_SERVER['DOCUMENT_ROOT'].'\MVC\Models\Model Common\model_info_user.php';
require_once $_SERVER['DOCUMENT_ROOT'].'\MVC\Models\Model DataLayer\db_query.php';
global $fullName;
global $gender;
global $dateOfBirth;
global $address;
global $email;
global $phoneNumber;
?>
<div class="container border mt-3 bg-light">
    <h2 class="pt-3">Thông tin người dùng</h2>
    <hr>
    <div class="">
        <form method="post" action="">
            <div class="form-group d-flex">
                <label class="col-sm-2  pt-2" for="full_name">Họ và tên:</label>
                <input value="<?php echo $fullName?>" type="text" class="form-control col-sm-6" id="full_name" placeholder="Nhập họ và tên" name="full_name">
            </div>
            <div class="form-group d-flex">
                <label class="col-sm-2  pt-2" for="gender">Giới tính:</label><br>
                <label>
                    <select name="gender" id="gender" class="form-control">
                        <option value="male" <?php if ($gender==1) echo "selected =\"selected\";" ?>>Nam</option>
                        <option value="female" <?php if ($gender==2) echo "selected =\"selected\";" ?>>Nữ</option>
                        <option value="other" <?php if ($gender==3) echo "selected =\"selected\";" ?>>Khác</option>
                    </select>
                </label>
            </div>
            <div class="form-group d-flex">
                <label class="col-sm-2 pt-2" for="date_of_birth">Ngày sinh:</label>
                <input type="date" class="form-control col-sm-6" id="date_of_birth" name="date_of_birth" value="<?php echo $dateOfBirth?>">
            </div>
            <div class="form-group d-flex">
                <label class="col-sm-2 pt-2" for="address">Địa chỉ:</label>
                <input value="<?php echo $address?>" type="text" class="form-control col-sm-6" id="address" placeholder="Nhập địa chỉ" name="address">
            </div>
            <div class="form-group d-flex">
                <label class="col-sm-2 pt-2" for="email">Email:</label>
                <input value="<?php echo $email?>" type="email" class="form-control col-sm-6" id="email" placeholder="Nhập email" name="email">
            </div>
            <div class="form-group d-flex">
                <label class="col-sm-2 pt-2" for="phone_number">Số điện thoại:</label>
                <input value="<?php echo $phoneNumber?>" type="text" class="form-control col-sm-6" id="phone_number" placeholder="Nhập số điện thoại" name="phone_number">
            </div>
            <button type="submit" class="btn btn-primary mb-2" name="update_user">Lưu</button>
            <?php
            require_once $_SERVER['DOCUMENT_ROOT'].'\MVC\Models\Model Form\model_update_user.php';
            ?>
        </form>
    </div>
</div>

</body>
</html>
