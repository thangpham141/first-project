<?php
require_once $_SERVER['DOCUMENT_ROOT'].'\MVC\Models\Model Common\model_get_info_user.php';

if(isset($_GET['id'])){
    $id = $_GET['id'];
    $infoUser = GetInfoUser($id);
    if(!empty($infoUser)){
        $fullName = $infoUser[0]['FullName'];
        $gender = $infoUser[0]['Gender'];
        $dateOfBirth = $infoUser[0]['DateOfBirth'];
        $address = $infoUser[0]['Address'];
        $email = $infoUser[0]['Email'];
        $phoneNumber = $infoUser[0]['PhoneNumber'];
    }
}